#!/bin/bash

set -ux
LANIF=eno1
VLANIF=${LANIF}.40
VLANMAC=44:99:33:66:11:10
VLANIP=172.168.10.10
VLANGW=$VLANIP
LTERULE=$(grep -c lte /etc/iproute2/rt_tables 2> /dev/null)

set -eux
sudo ip link add link $LANIF name $VLANIF address $VLANMAC type vlan id 40
sudo ip link set dev $VLANIF up
sudo ip addr add ${VLANIP}/24 dev $VLANIF

sudo ifconfig $VLANIF:m11 172.16.1.102 up
sudo ifconfig $VLANIF:m10 192.168.10.110 up
sudo ifconfig $VLANIF:sxu 172.55.55.102 up
sudo ifconfig $VLANIF:sxc 172.55.55.101 up
sudo ifconfig $VLANIF:s5c 172.58.58.102 up
sudo ifconfig $VLANIF:p5c 172.58.58.101 up
sudo ifconfig $VLANIF:s11 172.16.1.104 up

if [ $LTERULE = 0 ]; then
    echo '200 lte' | sudo tee --append /etc/iproute2/rt_tables
fi

sudo ip r add default via $VLANGW dev $VLANIF table lte
sudo ip rule add from 12.0.0.0/8 table lte
sudo ip rule add from 12.1.1.0/8 table lte
