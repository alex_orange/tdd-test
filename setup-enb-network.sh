#!/bin/bash

set -eux
LANIF=eno1
VLANIF=${LANIF}.40
VLANMAC=44:99:33:66:11:11
VLANIP=172.168.10.11

sudo ip link add link $LANIF name $VLANIF address $VLANMAC type vlan id 40
sudo ip link set dev $VLANIF up
sudo ip addr add ${VLANIP}/24 dev $VLANIF
